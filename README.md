# Cheatsheet - Webstránka #

### Autor ###
Juraj Garaj  
juraj982@gmail.com  
IV.IT
### Odkazy ###
[Dokumentácia](https://docs.google.com/document/d/10Z7Mzkcg2RlmORsAKUVn58MPvDfMoIpAJ5zfUT8685A/edit?usp=sharing)  
[PHP Docs](https://docs.google.com/document/d/1fGbMq4lN7uhp2ziRckU-iK533FfiaVUI2jC-9i0fUMM/edit?usp=sharing)  
[Databáza](https://docs.google.com/spreadsheets/d/1-t90kVGyY0ZxsgEbSAm-Fc6iuyEhZBwP4o7EAxm-jhk/edit?usp=sharing)  
[Notes](https://docs.google.com/document/d/1XSLSumqUYUt7CEm9m3Vezv9uYKQvCy0BOmnNN-Wep5I/edit?usp=sharing)